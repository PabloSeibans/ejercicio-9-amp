
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.css']
})
export class ComponentsComponent implements OnInit {

  form!: FormGroup;
  arrayutiles:string[]=[];
  

  constructor(private _fb: FormBuilder) { 
    this.crearFormulario();
  }
  
  ngOnInit(): void {
  }

  get invalido(){
    return this.form.invalid;
  }
  
  get utilesEscolares(){
    return this.form.get('utiles') as FormArray
  }

  crearFormulario(): void {
    this.form = this._fb.group({
      
      utiles: this._fb.array([])
    })
    this.utilesEscolares.push(this._fb.group({
      check: [null],
      utilNuevo: [null, Validators.pattern(/^[1-9-0-a-zA-zñÑ\s]+$/)]
    }))
  }

  adicionMaterial(): void{
    const nuevo = this._fb.group({
      check: [null],
      utilNuevo : [null,Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]
    })
    this.utilesEscolares.insert(0,nuevo);

  }

  
  borrarPasatiempo(i: number): void{
    this.utilesEscolares.removeAt(i)
  }

  eliminarall():void{
    this.utilesEscolares.clear()
  }

  guardar(): void {
    this.arrayutiles=[];
    for (let i = 0; i < this.utilesEscolares.length; i++) {
      if (this.utilesEscolares.at(i).get('check')?.value ==true &&  this.utilesEscolares.at(i).get('utilNuevo')?.value != null ) {
        this.arrayutiles.push( this.utilesEscolares.at(i).get('utilNuevo')?.value);
      }  
    }
    console.log(this.arrayutiles);
  }





  
}
